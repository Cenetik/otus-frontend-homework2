import React, { ComponentType } from 'react';
import LoginPage from '../Pages/Login/LoginPage';

interface AuthProps {
  isAuthenticated: boolean;
}

const withAuthentication = <P extends AuthProps>(
  WrappedComponent: ComponentType<P>
) => {
  return class HOC extends React.Component<Omit<P, keyof AuthProps> & AuthProps> {
    render() {
      const { isAuthenticated, ...props } = this.props;
      console.log('HOC');
      if(isAuthenticated)
        console.log('isAuthenticated');
      // Если пользователь аутентифицирован, рендерим обернутый компонент, иначе возвращаем null или что-то другое
      return isAuthenticated ? <WrappedComponent {...props as P} /> : <LoginPage/>;
    }
  };
};



export default withAuthentication;