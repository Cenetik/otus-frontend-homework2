import { AppBar, Button, Container, IconButton, Toolbar, Typography } from "@mui/material"
import { Link, useNavigate } from 'react-router-dom';

const Navigation = () => {
    const navigate = useNavigate();

    return  <AppBar position="static">
                <Toolbar>
                <IconButton
                    size="large"
                    edge="start"
                    color="inherit"
                    aria-label="menu"
                    sx={{ mr: 2 }}
                >            
                </IconButton>          
                <Button color="inherit" onClick = {(e)=>{navigate('/login')}}>Login</Button>
                <Button color="inherit" onClick = {(e)=>{navigate('/registration')}}>Registration</Button>          
                </Toolbar>
            </AppBar>    
};

export default Navigation;