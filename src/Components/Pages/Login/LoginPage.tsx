import './LoginPage.css';
import { Box, Button, Grid, TextField, Typography } from '@mui/material';
import { useState } from 'react';
import { connect, useDispatch } from "react-redux";
import { useNavigate } from 'react-router-dom';

const LoginPage = () => {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const [username, setUsername] = useState('');
  return (
    <div className="container">
      <Grid container direction="column" alignItems="center">
        <Grid item>
          <Typography variant="h4" className="formTitle">
            Логин
          </Typography>
        </Grid>
        <Grid item>
          <Box className="formContainer">
            <form>
              <Grid container direction="column" spacing={2}>
                <Grid item>
                  <TextField
                    label="Логин"
                    variant="outlined"
                    fullWidth
                    className="formInput"
                    value = {username}
                    onChange={(e)=>setUsername(e.target.value)}
                  />                  
                </Grid>
                <Grid item>
                  <TextField
                    label="Пароль"
                    variant="outlined"
                    fullWidth
                    className="formInput"
                  />
                </Grid>
                <Grid item>
                  <Button variant="contained" className="formButton" fullWidth onClick={
                                                                                        (e)=>{ 
                                                                                                dispatch({ type: '[BTN_EV] CHANGE_USER', payload: username 
                                                                                                         });
                                                                                                navigate('/');                                                                                                         
                                                                                             }}>
                    Войти
                  </Button>
                </Grid>
              </Grid>
            </form>
          </Box>
        </Grid>
      </Grid>
    </div>
  );
}

export default LoginPage;