import { Container, Typography } from '@mui/material';
import { connect } from "react-redux";
import { MyProps } from '../../../StateManagement/somereducer';
import withAuthentication from '../../WithAuthentication/WithAuthentication';

export interface HomeProps {
  username: string;  
  isAuthenticated: boolean; 
}

const HomePage = (props: HomeProps) => {
  return (
    
      <Container>
        <Typography variant="h3" component="h1">
          Домашняя страница
        </Typography>           
        
        <Typography variant="h5" component="h1">
          Добро пожаловать на домашнюю страницу, {props.username}!
        </Typography>
       
        </Container>      
  );
};

const mapReduxStateToProps = (globalReduxState: any): MyProps => {
      console.log('globalredux',globalReduxState);
      return {          
          username: globalReduxState.namesStore.username, 
          isAuthenticated: (globalReduxState.namesStore.username !== undefined && globalReduxState.namesStore.username!=='')
      };
  }

export default connect(mapReduxStateToProps)(withAuthentication(HomePage));