import { Box, Button, Typography } from "@mui/material";
import { purple } from "@mui/material/colors";
import { useNavigate } from "react-router-dom";

const primary = purple[600];

export function Page404() {
	const navigate = useNavigate();
	return   <Box
	sx={{
	  display: 'flex',
	  justifyContent: 'center',
	  alignItems: 'center',
	  flexDirection: 'column',
	  minHeight: '100vh'	
	}}
  >
	<Typography variant="h1" >
	  404
	</Typography>
	<Typography variant="h6" >
	  Страница не найдена.
	</Typography>
	<Button variant="contained" onClick={(e)=>navigate('/')}>Back Home</Button>
  </Box>        
}