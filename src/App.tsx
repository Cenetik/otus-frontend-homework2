import React from 'react';
import logo from './logo.svg';
import './App.css';

import { BrowserRouter, Link, Route, Routes, useNavigate } from 'react-router-dom';
import  ListItemButton  from '@mui/material/ListItemButton'
import { AppBar, Button, IconButton, ListItemText, Toolbar } from '@mui/material';
import { Page404 } from './Components/Pages/Page404/Page404';
import Register from './Components/Pages/Register/Register';
import LoginPage from './Components/Pages/Login/LoginPage';
import HomePage from './Components/Pages/Home/HomePage';
import Navigation from './Components/Navigation/Navigation';

function App() {  
  return ( 
    <BrowserRouter>
      <Navigation/>
      <Routes>
        <Route path='/' element={<HomePage/>} />  
        <Route path="/login" element={<LoginPage />} /> 
        <Route path="/registration" element={<Register />} />         
        <Route path="*" element={<Page404 />} />                     
      </Routes>
    </BrowserRouter>
  );
}

export default App;
