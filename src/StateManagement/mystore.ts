import { configureStore } from '@reduxjs/toolkit';
import somereducer from './somereducer';

const mystore = configureStore({
    reducer: {        
        namesStore: somereducer
    },

});
export default mystore;