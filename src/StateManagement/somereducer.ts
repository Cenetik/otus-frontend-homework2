export interface MyProps {   
    username: string;
    isAuthenticated: boolean; 
}

const initialState: MyProps = {  
    username: '',
    isAuthenticated: false
}

const somereducer = (state = initialState, action: any) => {
    console.log('dispatcher received');
    switch (action.type) {
        case '[BTN_EV] CHANGE_NAME':         
            console.log("is BTN_EV CHANGE_NAME");   
            return { ...state, btnName: action.payload };
        case '[BTN_EV] CHANGE_USER':         
            console.log("is BTN_EV CHANGE_USER, payload: "+action.payload);   
            return { ...state, username: action.payload };            
        case '[DV_EV] CHANGE_NAME':         
            console.log("is DV_EV CHANGE_NAME");   
            return { ...state, dvName: action.payload };
        default:
            return state;
    }
};
export default somereducer;